# -*- coding: utf-8 -*-
from setuptools import setup, find_packages
import os
dir_path =os.path.dirname(os.path.abspath(__file__))


setup(
    name='Integra',
    version='2.0.0',
    author='SMART - Sistema de Monitoramento e Avaliação dos Resultados do Telessaúde',
    author_email='allysonbarrosrn@gmail.com',
    packages=['integra', 'tests'],
    license='LICENSE.txt',
    description='Cliente REST para o SMART.',
    long_description=open(os.path.join(dir_path,'README.txt')).read(),
    install_requires=[ "requests >= 2.2.1" ]
)
