# -*- coding: utf-8 -*-
from integra import *
from integra.indicadores import *

URL = 'http://localhost:8000/'

def main():
    
    i = Integra('fa6b6daca91a5cc4ba975c3f97c98e2f585f282b')
    
    equipes_saude = EquipesSaude()
    equipes_saude.add("10", "1000", None, "ESF 1000", "2653982", None);
    equipes_saude.add("10", None, "1001", "ESF 1001", "2653982", None);
    equipes_saude.add("10", None, "1002", "ESF 1002", "2653982", None);
    equipes_saude.add("10", None, "1003", "ESF 1003", "2653982", None);
    dados_serializados_equipes = Integra.serializar(equipes_saude)
    print dados_serializados_equipes, 99999
    respostas_equipe = i.enviar_dados(URL + 'api/equipes/.json', dados_serializados_equipes)
    print respostas_equipe

    quadro_um = QuadroUm(1, 2, 3)
    quadro_um.add_avaliacao_estrutura("240810", 11, 12, 13)
    quadro_um.add_avaliacao_estrutura("240325", 11, 12, 13)
    quadro_um.add_profissionais_registrados("240810", "2251", 14)
    quadro_um.add_profissionais_registrados("240810", "2231", 15)
    
    quadro_dois = QuadroDois(4, 5, 6, 7)
    quadro_dois.add_solicitacoes_cat_profissional("2251", 21)
    quadro_dois.add_solicitacoes_cat_profissional("2231", 21)
    quadro_dois.add_solicitacoes_equipe("1000",None, 22)
    quadro_dois.add_solicitacoes_equipe("1001",None, 22)
    quadro_dois.add_solicitacoes_municipio("240810", 30)
    quadro_dois.add_solicitacoes_municipio("240325", 10)
    quadro_dois.add_solicitacoes_uf("24", 24)
    quadro_dois.add_solicitacoes_uf("25", 25)
    quadro_dois.add_solicitacoes_membro_gestao(None, "cns4321", "Nome Membro Gestão 1", "membro1@telessaude.ufrn.br", 24)
    quadro_dois.add_solicitacoes_membro_gestao("70030044450", "cns", "Nome Membro Gestão 2", "membro2@telessaude.ufrn.br", 24)
    quadro_dois.add_solicitacoes_ponto_telessaude("2653982", 26)
    quadro_dois.add_solicitacoes_ponto_telessaude("2408635", 2)
    quadro_dois.add_solicitacoes_profissional("70030044450", None, "Nome Profissional 1", "prof1@telessaude.ufrn.br", "01","223565","1000",None, 27); #tipo: 01 - Profissionais de Saúde
    quadro_dois.add_solicitacoes_profissional(None, None, "Nome Profissional 2", "prof2@telessaude.ufrn.br", "03","225125",None ,"1001",  28); #tipo: 03 - Mais Médicos
    quadro_dois.add_solicitacoes_profissional(None, "cns_1234", "Nome Profissional 3", "prof3@telessaude.ufrn.br", "02","225125",None,"1002",  28); #tipo: 02 - PROVAB
    quadro_dois.add_solicitacoes_profissional_tema("70030044450", None, None, "R05", "R05",10);
    quadro_dois.add_solicitacoes_profissional_tema(None, None, "prof2@telessaude.ufrn.br", "R070",  None,10);
    quadro_dois.add_solicitacoes_profissional_tema(None, "cns_1234", None, None, "D11",10);
    
    quadro_tres = QuadroTres(8, 9)
    quadro_tres.add_solicitacoes_telediagnostico_equipe("1000",None, 10)
    quadro_tres.add_solicitacoes_telediagnostico_equipe("1001",None, 11)
    quadro_tres.add_solicitacoes_telediagnostico_municipio("240810", 30)
    quadro_tres.add_solicitacoes_telediagnostico_municipio("240325", 10)
    quadro_tres.add_solicitacoes_telediagnostico_ponto_telessaude("2653982", 33)
    quadro_tres.add_solicitacoes_telediagnostico_ponto_telessaude("2408635", 33)
    quadro_tres.add_solicitacoes_telediagnostico_tipo("H21010137", 10)
    quadro_tres.add_solicitacoes_telediagnostico_tipo("H97019003", 10)
    quadro_tres.add_solicitacoes_telediagnostico_uf("24", 24)
    quadro_tres.add_solicitacoes_telediagnostico_uf("25", 25)
    
    quadro_quatro = QuadroQuatro(10, 11)
    quadro_quatro.add_atividades_realizadas_uf(24, 10)
    quadro_quatro.add_acessos_objetos_aprendizagem("24", "240810", "1000",None, "2653982", 41)
    quadro_quatro.add_acessos_objetos_aprendizagem("25", "250750", "1000",None, "2400103", 41)
    quadro_quatro.add_acessos_objetos_aprendizagem_cat_profissional("2251", 42)
    quadro_quatro.add_acessos_objetos_aprendizagem_cat_profissional("2231", 42)
    quadro_quatro.add_acessos_objetos_aprendizagem_equipe("1000",None, 100)
    quadro_quatro.add_acessos_objetos_aprendizagem_equipe("1001",None, 101)
    quadro_quatro.add_acessos_objetos_aprendizagem_municipio("240810", 44)
    quadro_quatro.add_acessos_objetos_aprendizagem_municipio("240325", 10)
    quadro_quatro.add_acessos_objetos_aprendizagem_ponto_telessaude("2653982", 45)
    quadro_quatro.add_acessos_objetos_aprendizagem_ponto_telessaude("2408635", 10)
    quadro_quatro.add_atividades_realizadas_equipe("1000",None, 50)
    quadro_quatro.add_atividades_realizadas_equipe("1001",None, 25)
    quadro_quatro.add_atividades_realizadas_municipio("240810", 40)
    quadro_quatro.add_atividades_realizadas_municipio("240325", 10)
    quadro_quatro.add_atividades_realizadas_ponto_telessaude("2653982", 30)
    quadro_quatro.add_atividades_realizadas_ponto_telessaude("2408635", 10)
    quadro_quatro.add_atividades_realizadas_uf("24", 50)
    quadro_quatro.add_atividades_realizadas_uf("25", 20)
    quadro_quatro.add_participantes_cat_profissional_equipe("1000",None, "2251", 100)
    quadro_quatro.add_participantes_cat_profissional_equipe("1000",None, "2231", 101)
    quadro_quatro.add_participantes_cat_profissional_municipio("240810", "2251", 50)
    quadro_quatro.add_participantes_cat_profissional_municipio("240325", "2231", 30)
    quadro_quatro.add_participantes_cat_profissional_ponto_telessaude("2653982", "2251", 52)
    quadro_quatro.add_participantes_cat_profissional_ponto_telessaude("2408635", "2231", 52)
    quadro_quatro.add_participantes_cat_profissional_uf("24", "2251", 50)
    quadro_quatro.add_participantes_cat_profissional_uf("25", "2231", 30)
    
    quadro_cinco = QuadroCinco(13, 14, 15, 16)
    quadro_cinco.add_evitacao_encaminhamento_cat_profissional("2231", 55.5, 44.5)
    quadro_cinco.add_evitacao_encaminhamento_cat_profissional("2251", 30.1, 69.9)
    quadro_cinco.add_cat_profissionais_frequentes("2251")
    quadro_cinco.add_cat_profissionais_frequentes("2235")
    quadro_cinco.add_especialidades_frequentes("225125")
    quadro_cinco.add_especialidades_frequentes("223565")
    quadro_cinco.add_resolucao_duvida(80.0, 10.0, 5.0, 5.0)
    quadro_cinco.add_satisfacao_solicitante("1", 60.0)
    quadro_cinco.add_satisfacao_solicitante("2", 35.0)
    quadro_cinco.add_satisfacao_solicitante("3", 5.0)
    quadro_cinco.add_satisfacao_solicitante("4", 0.0)
    quadro_cinco.add_satisfacao_solicitante("5", 0.0)
    quadro_cinco.add_temas_frequentes('R05', 'R05')
    quadro_cinco.add_temas_frequentes('R070', 'D11')
    
    quadro_seis = QuadroSeis()
    quadro_seis.add_avaliacao_satisfacao_objeto_aprendizagem("1", 60.0)
    quadro_seis.add_avaliacao_satisfacao_objeto_aprendizagem("2", 35.0)
    quadro_seis.add_avaliacao_satisfacao_objeto_aprendizagem("3", 5.0)
    quadro_seis.add_avaliacao_satisfacao_participantes("1", 60.0)
    quadro_seis.add_avaliacao_satisfacao_participantes("2", 35.0)
    quadro_seis.add_avaliacao_satisfacao_participantes("3", 5.0)
    quadro_seis.add_temas_frequentes_objeto_aprendizagem("1")
    quadro_seis.add_temas_frequentes_objeto_aprendizagem("2")
    quadro_seis.add_temas_frequentes_participacao("1")
    quadro_seis.add_temas_frequentes_participacao("2")
    
    indicador = IndicadorGeral(10, "012013", quadro_um, quadro_dois, quadro_tres, quadro_quatro, quadro_cinco, quadro_seis)       
    dados_serializados = Integra.serializar(indicador) 

    respostas = str(i.enviar_dados(URL + 'api/indicadores/.json', dados_serializados)) 
    print respostas
    
if __name__ == "__main__":
    main()
