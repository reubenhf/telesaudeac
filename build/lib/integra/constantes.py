# -*- coding: utf-8 -*-

class TipoTeleconsultoria:
    ASSINCRONA = "A"
    SINCRONA = "S"

class CanalAcesso:
    INTERNET = "1"
    TELEFONE = "2"

class GrauSatisfacao:
    MUITO_INSATISFEITO = "1"
    INSATISFEITO = "2"
    INDIFERENTE = "3"
    SATISFEITO = "4"
    MUITO_SATISFEITO = "5"
    NAO_INFORMADO = "9"

class ResolucaoDuvidaTeleconsultoria:
    ATENDEU_TOTALMENTE = "1"
    ATENDEU_PARCIALMENTE = "2"
    NAO_ATENDEU = "3"
    NAO_INFORMADO = "9"

class IntencaoEncaminhamentoTeleconsultoria:
    NAO = "0"
    SIM = "1"
    NAO_INFORMADO = "9"

class EvitouEncaminhamentoTeleconsultoria:
    NAO = "0"
    SIM = "1"
    NAO_INFORMADO = "9"

class TipoAtividade:
    CURSO = "1"
    WEBAULAS_PALESTRAS = "2"
    WEBSEMINARIOS = "3"
    FORUM = "4"
    REUNIAO = "5"

class TipoObjetoAprendizagem:
    TEXTO = "1"
    MULTIMIDIA = "2"
    IMAGENS = "3"
    APLICATIVOS = "4"
    JOGOS_EDUCACIONAIS = "5"
    OUTROS = "6"

class Sexo:
    FEMININO = "F"
    MASCULINO = "M"
