.. integra documentation master file, created by
   sphinx-quickstart on Wed Jul 23 22:06:48 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Módulo Facilitador de Integração - v2.0
=======================================

Conteúdo:

.. toctree::
   :maxdepth: 4

   integra
   constants
   tests


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
