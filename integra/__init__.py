# -*- coding: utf-8 -*-
import requests
import json
import datetime
from minify import json_minify

class Integra:
    """ Empacota os dados e os envia para o webservice.
            Inicializa uma nova instância da classe com código do Token gerado pelo sistema.

        :param token_autenticacao: Token gerado pelo sistema, representa um coordenador ou técnico de Núcleo de Telessaúde.
        :type token_autenticacao: str

        """

    def __init__(self, token_autenticacao):
        self.token_autenticacao = token_autenticacao

    @staticmethod
    def __to_dict(obj, classkey=None):
        if isinstance(obj, dict):
            data = {}
            for (k, v) in obj.items():
                data[k] = Integra.__to_dict(v, classkey)
            return data
        elif hasattr(obj, "_ast"):
            return Integra.__to_dict(obj._ast())
        elif hasattr(obj, "__iter__"):
            return [Integra.__to_dict(v, classkey) for v in obj]
        elif hasattr(obj, "__dict__"):
            data = dict([(key, Integra.__to_dict(value, classkey))
                for key, value in obj.__dict__.iteritems()
                if not callable(value) and not key.startswith('_')])
            if classkey is not None and hasattr(obj, "__class__"):
                data[classkey] = obj.__class__.__name__
            return data
        else:
            return obj

    def enviar_dados(self, url, dados):
        """ Método utilizado para enviar as informações de indicadores para o webservice.
            Envia o os dados para o serviço especificado na url

        :param url: URL do webservice
        :type url: str
        :param dados: Objeto com os dados a serem enviados
        :type dados: str

        """

        if (self.token_autenticacao is not None and self.token_autenticacao is not ""):
            try:
                headers = {
                           'Authorization': 'Token ' + self.token_autenticacao,
                           'content-type' : 'application/json'
                           }

                r = requests.post(url, data=dados, headers=headers)
                return r.json()
            except Exception:
                return "Atenção: Não foi possível conectar-se à URL informada."
        else:
            return "Atenção: Você precisa setar o token de autenticação."

    @staticmethod
    def serializar(dados):
        """Converte os indicadores no tipo do formato aceito pelo webservice.

        :param dados: Dados a serem serializados para JSON
        :type dados: IndicadorGeral ou EquipesSaude
        :returns: str -- dados formatados em JSON

        """

        try:
            return json_minify(json.dumps(Integra.__to_dict(dados)))
        except Exception as e:
            # return "Atenção: Não foi possível serializar os dados para o formato JSON."
            return e.message

    @staticmethod
    def validate_date(date, format='%d/%m/%Y %H:%M:%S'):
        try:
            datetime.datetime.strptime(date, format)
            return True
        except ValueError:
            return False


class EquipesSaude:
    """Representa uma coleção de dados cadastrais de equipes de saúde.
    """

    def __init__(self):
        self.list = []

    def add(self, codigo_nucleo, codigo_equipe, codigo_equipe_ine, nome, cnes_estabelecimento, codigo_tipo_equipe):
        """ Adiciona os dados cadastrais de uma equipe de saúde.

        .. note::
            Pelo menos um dos dois (codigo_equipe ou codigo_equipe_ine)
            deve ser informado.

        :param codigo_nucleo: Código de identificação do Núcleo de Telessaúde. Consulta código no sistema INTEGRA.
        :type codigo_nucleo: str
        :param codigo_equipe: Código de identificação de equipe de saúde utilizada pelo sistema de teleconsultoria (máximo 12 caracteres).
        :type codigo_equipe: str
        :param codigo_equipe_ine: Código Identificador Nacional de Equipe (INE)(máximo 12 caracteres).
        :type codigo_equipe_ine: str
        :param nome: Nome da equipe de saúde (máximo 60 caracteres).
        :type nome: str
        :param cnes_estabelecimento: Código Cadastro Nacional de Estabelecimento de Saúde (CNES).
        :type cnes_estabelecimento: str
        :param codigo_tipo_equipe: Código do tipo de equipe. Consulta código no sistema.
        :type codigo_tipo_equipe: str

        """

        self.list.append({
                     "codigo_nucleo" : codigo_nucleo,
                     "codigo_equipe" : codigo_equipe,
                     "codigo_equipe_ine" : codigo_equipe_ine,
                     "codigo_tipo_equipe" : codigo_tipo_equipe,
                     "nome" : nome,
                     "cnes_estabelecimento" : cnes_estabelecimento
                     })

    def _ast(self):
        return self.list


class ProfissionalSaude:

    """Classe responsável por cadastrar/atualizar os dados de profissional de saúde.
    Quando o SMART recebe um CPF ele tentará buscar na base e se não encontrado irá buscar na base do CNES.
    Esse serviço só será utilizado caso o SMART não consiga encontrar o profissional pelo CPF informado nos outros serviços.

    :param codigo_nucleo: Código CNES de identificação do núcleo cadastrado no SMART.
    :type codigo_nucleo: str

    :param mes_referencia: Mês de referência para os indicadores informados.
    :type mes_referencia: str

    """

    def __init__(self, codigo_nucleo, mes_referencia):
        self.codigo_nucleo = codigo_nucleo
        self.mes_referencia = mes_referencia
        self.profissionais = []

    def addProfissionalSaude(self, codigo_cns, codigo_cpf, nome, estabelecimento, ocupacao, equipe_ine, tipo_profissional, sexo):
        """
        Adicona os dados básicos do profissional de saúde e seu respectivo vínculo.


        :param codigo_cns: (opcional) CNS do profissional
        :type codigo_cns: str

        :param codigo_cpf: CPF do profissional
        :type codigo_cpf: str

        :param nome: Nome do profissional
        :type nome: str

        :param estabelecimento: Código CNES do estabelecimento de saúde no qual o profissional solicitante atua
        :type estabelecimento: str

        :param ocupacao: Código CBO da ocupação. Consultar lista de CBOs disponível no SMART através do menu "Cadastros Gerais > Especialidades (CBO)".
        :type ocupacao: str

        :param equipe_ine: (opcional) Código INE da equipe de saúde da qual o profissional faz parte
        :type equipe_ine: str

        :param tipo_profissional: Código do tipo de profissional. Consultar lista de tipos de profissionais disponível no SMART através do menu "Cadastros Gerais > Tipos de Profissionais".
        :type tipo_profissional: str

        :param sexo: Sexo do profissional de saúde
        :type sexo: str

        """


        self.profissionais.append({
             "cns": codigo_cns,
             "cpf": codigo_cpf,
             "nome": nome,
             "cnes": estabelecimento,
             "cbo": ocupacao,
             "ine": equipe_ine,
             "tprof": tipo_profissional,
             "sexo": sexo
        })


class EstabelecimentoSaude:
    """Classe responsável por atualizar os estabelecimentos de saúde quanto ao serviço consumido.


    :param codigo_nucleo: Código CNES de identificação do núcleo cadastrado no SMART.
    :type codigo_nucleo: str
    :param mes_referencia: Mês de referência para os indicadores informados.
    :type mes_referencia: str

    """

    def __init__(self, codigo_nucleo, mes_referencia):
        self.codigo_nucleo = codigo_nucleo
        self.mes_referencia = mes_referencia
        self.estabelecimentos = []

    def atualizarEstabelecimentoSaude(self, codigo_cnes, cadastrado_servico_teleconsultoria, cadastrado_servico_teleeducacao, cadastrado_servico_telediagnostico):
        """ Adiciona o estabelecimento de saúde que será atualizado.

        :param codigo_cnes: Código CNES do estabelecimento de saúde no qual o profissional solicitante atua
        :type codigo_cnes: str
        :param cadastrado_servico_teleconsultoria: Se o estabelecimento consome serviço de Teleconsultoria
        :type cadastrado_servico_teleconsultoria: str
        :param cadastrado_servico_teleeducacao: Se o estabelecimento consome serviço de tele-educação
        :type cadastrado_servico_teleeducacao: str
        :param cadastrado_servico_telediagnostico: Se o estabelecimento consome serviço de Telediagnóstico
        :type cadastrado_servico_telediagnostico: str


        """
        self.estabelecimentos.append({
             "cnes": codigo_cnes,
             "tconsul": "1" if cadastrado_servico_teleconsultoria else "0",
             "teduca": "1" if cadastrado_servico_teleeducacao else "0",
             "tdiagn": "1" if cadastrado_servico_telediagnostico else "0",
        })


class Teleconsultoria:
    """Classe responsável por armazenar as solicitações de teleconsultoria.


    :param codigo_nucleo: Código CNES de identificação do núcleo cadastrado no SMART.
    :type codigo_nucleo: str
    :param mes_referencia: Mês de referência para os indicadores informados.
    :type mes_referencia: str

    """

    def __init__(self, codigo_nucleo, mes_referencia):
        self.codigo_nucleo = codigo_nucleo
        self.mes_referencia = mes_referencia
        self.teleconsultorias = []

    def addTeleconsultoria(self, dh_solicitacao, tipo, canal_acesso_sincrona, cpf_solicitante, especialidade_solicitante, ponto_telessaude_solicitacao, equipe_do_solicitante, tipo_profissional, cids, ciaps, dh_resposta_solicitacao, evitou_encaminhamento, intencao_encaminhamento, grau_satisfacao, resolucao_duvida, potencial_sof):
        """ Adiciona a solicitação de teleconsultoria.
            O SMART considera uma teleconsultoria única pela chave  (dh_solicitacao e cpf_solicitante)

        :param dh_solicitacao: Data/hora da solicitação da teleconsultoria no formato dd/MM/yyyy HH:MM:SS
        :type dh_solicitacao: str

        :param tipo: Tipo da solicitação
        :type tipo: str

        :param canal_acesso_sincrona: Canal de acesso
        :type canal_acesso_sincrona: str

        :param cpf_solicitante: CPF do profissional que solicitou a teleconsultoria
        :type cpf_solicitante: str

        :param especialidade_solicitante: Código CBO da ocupação do solicitante no momento da solicitação da teleconsultoria. Consultar lista de CBOs dispnonível no SMART através do menu "Cadastros Gerais > Especialidades (CBO)".
        :type especialidade_solicitante: str

        :param ponto_telessaude_solicitacao: Código CNES do estabelecimento de saúde no qual o profissional solicitante atua
        :type ponto_telessaude_solicitacao: str

        :param equipe_do_solicitante: (opcional) Código INE da equipe de saúde na qual o profissional solicitante faz parte
        :type equipe_do_solicitante: str

        :param tipo_profissional: Código do tipo de profissional. Consultar lista de tipos de profissionais disponível no SMART através do menu "Cadastros Gerais > Tipos de Profissionais".
        :type tipo_profissional: str

        :param cids: Lista com os códigos CID (Classificação Internacional de Doenças). Consultar lista de CIDs disponível no SMART através do menu "Cadastros Gerais > CID 10 - Classificação Internacional de Doenças".
        :type cids: str

        :param ciaps: Lista com os códigos CIAP (Classificação Internacional de Assistência Primária). Consultar lista de CIAPS disponível no SMART através do menu "Cadastros Gerais > CIAP 2 - Classificação Internacional de Atenção Primária".
        :type ciaps: str

        :param dh_resposta_solicitacao: Data/hora da resposta da solicitação no formato dd/MM/yyyy HH:MM:SS
        :type dh_resposta_solicitacao: str

        :param evitou_encaminhamento: Se a teleconsultoria evitou o encaminhamento de paciente
        :type evitou_encaminhamento: str

        :param intencao_encaminhamento: Se o profissional registrou na teleconsultoria que tinha intenção de encaminhar o paciente
        :type intencao_encaminhamento: str

        :param grau_satisfacao: Grau de satisfação do solicitante quanto a resposta da sua teleconsultoria
        :type grau_satisfacao: str

        :param resolucao_duvida: Se a resposta da teleconsultoria atendeu ou não a teleconsultoria
        :type resolucao_duvida: str

        :param potencial_sof: Se a teleconsultoria tem potencial para se transformar em SOF
        :type potencial_sof: str


        """
        teleconsultoria = {}

        if not Integra.validate_date(dh_solicitacao):
            raise Exception(u"A data da solicitação informada não está no formato dd/MM/yyyy HH:mm:ss.")

        if not Integra.validate_date(dh_resposta_solicitacao):
            raise Exception(u"A data da resposta informada não está no formato dd/MM/yyyy HH:mm:ss.")

        teleconsultoria["dtsol"] = dh_solicitacao
        teleconsultoria["tipo"] = tipo
        teleconsultoria["canal"] = canal_acesso_sincrona
        teleconsultoria["scpf"] = cpf_solicitante
        teleconsultoria["scbo"] = especialidade_solicitante
        teleconsultoria["scnes"] = ponto_telessaude_solicitacao
        if equipe_do_solicitante:
            teleconsultoria["sine"] = equipe_do_solicitante
        teleconsultoria["stipo"] = tipo_profissional
        if cids:
            teleconsultoria["cids"] = cids
        if ciaps:
            teleconsultoria["ciaps"] = ciaps
        teleconsultoria["dtresp"] = dh_resposta_solicitacao
        teleconsultoria["evenc"] = evitou_encaminhamento
        teleconsultoria["inenc"] = intencao_encaminhamento
        teleconsultoria["satisf"] = grau_satisfacao
        teleconsultoria["rduvida"] = resolucao_duvida
        teleconsultoria["psof"] = "1" if potencial_sof else "0"

        self.teleconsultorias.append(teleconsultoria)


class Telediagnostico:
    """Classe responsável por armazenar as solicitações de telediagnóstico


    :param codigo_nucleo: Código CNES de identificação do núcleo cadastrado no SMART.
    :type codigo_nucleo: str
    :param mes_referencia: Mês de referência para os indicadores informados.
    :type mes_referencia: str

    """

    def __init__(self, codigo_nucleo, mes_referencia):
        self.codigo_nucleo = codigo_nucleo
        self.mes_referencia = mes_referencia
        self.telediagnosticos = []

    def addSolicitacao(self, dh_realizacao_exame, codigo_tipo_exame, codigo_equipamento, tipo_justificativa, ponto_telessaude_com_telediagnostico, cpf_solicitante, especialidade_solicitante, ponto_telessaude_solicitacao, dh_laudo, cpf_laudista, especialidade_laudista, ponto_telessaude_laudista, cpf_paciente, cns_paciente, cidade_moradia_paciente):
        """ Adiciona a solicitação do telediagnóstico
            O SMART considera um telediagnóstico único pela chave  (dh_realizacao_exame e cpf_solicitante)

        :param dh_realizacao_exame: Data/hora da solicitação do telediagnóstico no formato dd/MM/yyyy HH:MM:SS
        :type dh_realizacao_exame: str

        :param codigo_tipo_exame: Código SIA/SIH do tipo do exame
        :type codigo_tipo_exame: str

        :param codigo_equipamento: (opcional) Código do equipamento utilizado para realizar o exame de telediagnóstico. Consultar lista de equipamentos disponível no SMART através do menu "Cadastros Gerais > Equipamentos".
        :type codigo_equipamento: str

        :param tipo_justificativa: (opcional) Código da justificativa utilizada caso o código do equipamento não tenha sido informado. Obrigatório se codigo_equipamento não foi fornecido
        :type tipo_justificativa: str

        :param ponto_telessaude_com_telediagnostico: Código CNES do estabelecimento de saúde onde está o equipamento que realiza o exame
        :type ponto_telessaude_com_telediagnostico: str

        :param cpf_solicitante: CPF do profissional que solicitou o telediagnóstico
        :type cpf_solicitante: str

        :param especialidade_solicitante: Código CBO da ocupação do solicitante no momento da solicitação do Telediagnóstico. Consultar lista de CBOs disponível no SMART através do menu "Cadastros Gerais > Especialidades (CBO)".
        :type especialidade_solicitante: str

        :param ponto_telessaude_solicitacao: Código CNES do estabelecimento de saúde no qual o profissional solicitante atua
        :type ponto_telessaude_solicitacao: str

        :param dh_laudo: Data/hora da disponibilização do laudo no formato dd/MM/yyyy HH:MM:SS
        :type dh_laudo: str

        :param cpf_laudista: CPF do especialista que elaborou o laudo
        :type cpf_laudista: str

        :param especialidade_laudista: Código CBO da ocupação do laudista.
        :type especialidade_laudista: str

        :param ponto_telessaude_laudista: Código CNES do laudista
        :type ponto_telessaude_laudista: str

        :param cpf_paciente: CPF do paciente
        :type cpf_paciente: str

        :param cns_paciente: (opcional) CNS do paciente. Obrigatório se CPF não foi fornecido
        :type cns_paciente: str

        :param cidade_moradia_paciente: Código IBGE sem o dígito verificador da cidade onde o paciente mora.
        :type cidade_moradia_paciente: str

        """

        telediagnostico = {}

        if not Integra.validate_date(dh_realizacao_exame):
            raise Exception(u"A data da realização do exame informada não está no formato dd/MM/yyyy HH:mm:ss.")

        if not Integra.validate_date(dh_laudo):
            raise Exception(u"A data de laudagem informada não está no formato dd/MM/yyyy HH:mm:ss.")

        telediagnostico["dhrexame"] = dh_realizacao_exame
        telediagnostico["ctexame"] = codigo_tipo_exame
        telediagnostico["cequipa"] = codigo_equipamento
        telediagnostico["tjust"] = tipo_justificativa
        telediagnostico["pnt"] = ponto_telessaude_com_telediagnostico
        telediagnostico["scpf"] = cpf_solicitante
        telediagnostico["scbo"] = especialidade_solicitante
        telediagnostico["scnes"] = ponto_telessaude_solicitacao
        telediagnostico["dhla"] = dh_laudo
        telediagnostico["lcpf"] = cpf_laudista
        telediagnostico["lcbo"] = especialidade_laudista
        telediagnostico["lcnes"] = ponto_telessaude_laudista
        telediagnostico["pcpf"] = cpf_paciente
        telediagnostico["pacns"] = cns_paciente
        telediagnostico["paibge"] = cidade_moradia_paciente

        self.telediagnosticos.append(telediagnostico)


class TeleeducacaoAtividade:
    """Classe responsável por armazenar os dados de atividades de tele-educação bem como as participações nessas atividades


    :param codigo_nucleo: Código CNES de identificação do núcleo cadastrado no SMART.
    :type codigo_nucleo: str
    :param mes_referencia: Mês de referência para os indicadores informados.
    :type mes_referencia: str

    """

    def __init__(self, codigo_nucleo, mes_referencia):
        self.codigo_nucleo = codigo_nucleo
        self.mes_referencia = mes_referencia
        self.atividades_teleeducacao = []

    def _findAtividadeInListByCodigoIdentificacao(self, codigo_identificacao):
        atividade_teleeducacao = None

        for atividade_item in self.atividades_teleeducacao:
            if codigo_identificacao == atividade_item["id"]:
                atividade_teleeducacao = atividade_item
                break

        if not atividade_teleeducacao:
            raise Exception(u"Atividade de Teleeducação não encontrada.")

        return atividade_item

    def addAtividade(self, codigo_identificacao, data_disponibilizacao, carga_horaria, tipo_atividade, tema_codigo_decs):
        """ Adiciona a atividade de tele-educação

        :param codigo_identificacao: Código único utilizado pelo núcleo para identificar atividade
        :type codigo_identificacao: str

        :param data_disponibilizacao: Data/hora em que a atividade foi disponibilizada no formato d/MM/yyyy HH:mm:ss
        :type data_disponibilizacao: str

        :param carga_horaria: Duração da atividade em minutos
        :type carga_horaria: str

        :param tipo_atividade: Tipos de atividades educacional
        :type tipo_atividade: str

        :param tema_codigo_decs: Código da classificação do Descritores em Ciências da Saude (DeCS) da BIREME. Consultar lista de DeCS dispnonível no SMART através do menu "Cadastros Gerais > deSc BIREME - Descritores".
        :type tema_codigo_decs: str
        """

        atividade_teleeducacao = {}

        if not Integra.validate_date(data_disponibilizacao):
            raise Exception(u"A data de disponibilização informada não está no formato dd/MM/yyyy HH:mm:ss.")

        atividade_teleeducacao["id"] = codigo_identificacao
        atividade_teleeducacao["dtdispo"] = data_disponibilizacao
        atividade_teleeducacao["cargah"] = carga_horaria
        atividade_teleeducacao["tipo"] = tipo_atividade
        atividade_teleeducacao["decs"] = tema_codigo_decs

        self.atividades_teleeducacao.append(atividade_teleeducacao)

    def addParticipacaoAtividade(self, codigo_identificacao, data_da_participacao, cpf_participante, especialidade_participante, estabelecimento_telessaude_participante, equipe_participante, grau_satisfacao):
        """ Adiciona as participações em atividades de tele-educação
            Observação: deve-se antes registrar a atividade de tele-educação.

        :param codigo_identificacao: Código único utilizado pelo núcleo para identificar atividade
        :type codigo_identificacao: str

        :param data_da_participacao: Data/hora da participação no formato dd/MM/yyyy HH:MM:SS
        :type data_da_participacao: str

        :param cpf_participante: CPF do participante da atividade
        :type cpf_participante: str

        :param especialidade_participante: Código CBO da ocupação do participante no momento da participação da atividade.
        :type especialidade_participante: str

        :param estabelecimento_telessaude_participante: Código CNES do estabelecimento de saúde no qual o participante atua no momento da participação da atividade
        :type estabelecimento_telessaude_participante: str

        :param equipe_participante: (opcional) Código INE da equipe de saúde da qual o participante faz parte
        :type equipe_participante: str

        :param grau_satisfacao: Grau de satisfação do participante quanto à atividade
        :type grau_satisfacao: str


        """

        participacao_atividade_teleeducacao = {}

        if not self.atividades_teleeducacao:
            raise Exception(u"Deve existir ao menos uma atividade registrada para o código de identificação fornecido.")

        if not codigo_identificacao:
            raise Exception(u"Código de identificação da atividade é obrigatório.")

        if not Integra.validate_date(data_da_participacao):
            raise Exception(u"A data de participação informada não está no formato dd/MM/yyyy HH:mm:ss.")

        participacao_atividade_teleeducacao["id"] = codigo_identificacao
        participacao_atividade_teleeducacao["dtparti"] = data_da_participacao
        participacao_atividade_teleeducacao["cpf"] = cpf_participante
        participacao_atividade_teleeducacao["cbo"] = especialidade_participante
        participacao_atividade_teleeducacao["cnes"] = estabelecimento_telessaude_participante
        if equipe_participante:
            participacao_atividade_teleeducacao["ine"] = equipe_participante
        participacao_atividade_teleeducacao["satisf"] = grau_satisfacao

        atividade_teleeducacao = self._findAtividadeInListByCodigoIdentificacao(codigo_identificacao)
        if not atividade_teleeducacao.get("participacoes_teleeducacao"):
            atividade_teleeducacao["participacoes_teleeducacao"] = []
        atividade_teleeducacao["participacoes_teleeducacao"].append(participacao_atividade_teleeducacao)


class TeleeducacaoObjetoAprendizagem:
    """Classe responsável por armazenar os dados de objetos de aprendizagem de tele-educação e seus respectivos acessos para monitoramento e avaliação do Programa Nacional Telessaúde Brasil Redes).


    :param codigo_nucleo: Código CNES de identificação do núcleo cadastrado no SMART.
    :type codigo_nucleo: str
    :param mes_referencia: Mês de referência para os indicadores informados.
    :type mes_referencia: str

    """

    def __init__(self, codigo_nucleo, mes_referencia):
        self.codigo_nucleo = codigo_nucleo
        self.mes_referencia = mes_referencia
        self.objetos_aprendizagem = []

    def addObjetoAprendizagem(self, codigo_identificacao, data_disponibilizacao, disponibilizado_plataforma_telessaude, disponibilizado_ares, disponibilizado_avasus, disponibilizado_redes_sociais, disponibilizado_outros, tipo_objeto, tema_codigo_decs, url, numero_acesso):
        """ Adiciona os objetos de aprendizagem disponibilizados.
            São considerados objetos de aprendizagem as ofertas de tele-educação  disponibilizadas de forma assíncronas em documento texto ou audiovisual  para acesso de profissional de saúde (vide nota técnica 50/2015 DEGES/SGTES/MS)

        :param codigo_identificacao: Código único utilizado pelo núcleo para identificar o objeto de aprendizagem
        :type codigo_identificacao: str

        :param data_disponibilizacao: Data/hora em que o objeto de aprendizagem foi disponibilizado no formato d/MM/yyyy HH:mm:ss
        :type data_disponibilizacao: str

        :param disponibilizado_plataforma_telessaude: Se disponibilizado na plataforma de telessaúde do próprio núcleo
        :type disponibilizado_plataforma_telessaude: str

        :param disponibilizado_ares: Se disponibilizado Biblioteca Virtual, Coletânea Telessaúde no ARES/UNA-SUS
        :type disponibilizado_ares: str

        :param disponibilizado_avasus: Se disponibilizado no AVA-SUS - Ambiente Virtual de Aprendizagem do Sistema Único de Saúde (SUS),
        :type disponibilizado_avasus: str

        :param disponibilizado_redes_sociais: Se disponibilizado em alguma rede social
        :type disponibilizado_redes_sociais: str

        :param disponibilizado_outros: Se disponibilizado em outro repositório
        :type disponibilizado_outros: str

        :param tipo_objeto: Classificação do objeto de aprendizagem
        :type tipo_objeto: str

        :param tema_codigo_decs: Código da classificação do Descritores em Ciências da Saude (DeCS) da BIREME. Consultar lista de DeCS disponível no SMART através do menu "Cadastros Gerais > deSc BIREME - Descritores".
        :type tema_codigo_decs: str

        :param url: Endereço de rede para acesso ao recurso quando este for público, não necessita de credenciais para acesso.
        :type url: str

        :param numero_acesso: Número de acesso ao objeto de aprendizagem
        :type numero_acesso: str

        """

        objeto_aprendizagem = {}

        if not codigo_identificacao:
            raise Exception(u"Código de identificação da atividade é obrigatório.")

        if not Integra.validate_date(data_disponibilizacao):
            raise Exception(u"A data de disponibilização informada não está no formato dd/MM/yyyy HH:mm:ss.")

        objeto_aprendizagem["id"] = codigo_identificacao
        objeto_aprendizagem["dtdispo"] = data_disponibilizacao
        objeto_aprendizagem["dplataf"] = "1" if disponibilizado_plataforma_telessaude else "0"
        objeto_aprendizagem["dares"] = "1" if disponibilizado_ares else "0"
        objeto_aprendizagem["davasus"] = "1" if disponibilizado_avasus else "0"
        objeto_aprendizagem["drsociais"] = "1" if disponibilizado_redes_sociais else "0"
        objeto_aprendizagem["doutros"] = "1" if disponibilizado_outros else "0"
        objeto_aprendizagem["tipo"] = tipo_objeto
        objeto_aprendizagem["decs"] = tema_codigo_decs
        if url:
            objeto_aprendizagem["url"] = url
        objeto_aprendizagem["num"] = numero_acesso

        self.objetos_aprendizagem.append(objeto_aprendizagem)

class TeleeducacaoCurso:
    """Classe responsável por armazenar os cursos oferecidos por meio da Tele-educação


    :param codigo_nucleo: Código CNES de identificação do núcleo cadastrado no SMART.
    :type codigo_nucleo: str
    :param mes_referencia: Mês de referência para os indicadores informados.
    :type mes_referencia: str

    """

    def __init__(self, codigo_nucleo, mes_referencia):
        self.codigo_nucleo = codigo_nucleo
        self.mes_referencia = mes_referencia
        self.cursos_teleeducacao = []

    def addCurso(self, identificacao_curso, data_inicio, data_fim, vagas_ofertadas, tema, carga_horaria, lista_cpf_matriculados, lista_cpf_formados, lista_cpf_evadidos, lista_cpf_reprovados):
        """ Adiciona/atualiza o curso oferecido pela tele-educação
            O SMART considera um curso único pela chave  (identificacao_curso e data_inicio)

        :param identificacao_curso: Código único utilizado pelo núcleo para identificar a disponibilização do curso
        :type identificacao_curso: str

        :param data_inicio: Data/hora no qual o curso foi disponibilizado formato d/MM/yyyy HH:mm:ss
        :type data_inicio: str

        :param data_fim: (opcional)Data/hora no qual o curso foi encerrado formato d/MM/yyyy HH:mm:ss
        :type data_fim: str

        :param vagas_ofertadas: Quantidade de vagas ofertas
        :type vagas_ofertadas: str

        :param tema: Código da classificação do Descritores em Ciências da Saude (DeCS) da BIREME. Consultar lista de DeCS disponível no SMART através do menu "Cadastros Gerais > deSc BIREME - Descritores".
        :type tema: str

        :param carga_horaria: Duração do curso em minutos
        :type carga_horaria: str

        :param lista_cpf_matriculados: (opcional) Lista de CPFs dos alunos matriculados. Quando encerrar o período de matrículas do curso, deve-se enviar novamente o curso com a relação dos alunos matriculados.
        :type lista_cpf_matriculados: str

        :param lista_cpf_formados: (opcional) Lista de CPFs dos alunos formados. Quando o curso tiver sido encerrado, deve-se enviar novamente o curso com a relação dos alunos formados.
        :type lista_cpf_formados: str

        :param lista_cpf_evadidos: (opcional) Lista de CPFs dos alunos evadidos. Quando o curso tiver sido encerrado, deve-se enviar novamente o curso com a relação dos alunos evadidos.
        :type lista_cpf_evadidos: str

        :param lista_cpf_reprovados: (opcional) Lista de CPFs dos alunos reprovados. Quando o curso tiver sido encerrado, deve-se enviar novamente o curso com a relação dos alunos reprovados.
        :type lista_cpf_reprovados: str

        """

        curso = {}

        if not identificacao_curso:
            raise Exception(u"Código de identificação do curso é obrigatório.")

        if not Integra.validate_date(data_inicio):
            raise Exception(u"A data de início do curso informada não está no formato dd/MM/yyyy HH:mm:ss.")

        if not Integra.validate_date(data_fim):
            raise Exception(u"A data de encerramento do curso informada não está no formato dd/MM/yyyy HH:mm:ss.")

        curso["id"] = identificacao_curso
        curso["dtini"] = data_inicio

        if data_fim:
            curso["dtfim"] = data_fim

        curso["vagas"] = vagas_ofertadas
        curso["decs"] = tema
        curso["cargah"] = carga_horaria
        curso["cpfs_matri"] = lista_cpf_matriculados
        curso["cpfs_forma"] = lista_cpf_formados
        curso["cpfs_evadi"] = lista_cpf_evadidos
        curso["cpfs_repro"] = lista_cpf_reprovados

        self.cursos_teleeducacao.append(curso)
