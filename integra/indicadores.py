# -*- coding: utf-8 -*-

class IndicadorGeral:
    """ Determina quais são os indicadores (quadros) a serem enviados.
    
    :param codigo_nucleo: Código de identificação do Núcleo de Telessaúde
    :type codigo_nucleo: str
    
    :param mes_referencia: Mês de referência para os indicadores informados
    :type mes_referencia: str
    
    :param quadro_um: Informa os indicadores relativos ao Quadro 1
        da nota técnica
    :type quadro_um: QuadroUm
    
    :param quadro_dois: Informa os indicadores relativos ao Quadro 2
        da nota técnica
    :type quadro_dois: QuadroDois
    
    :param quadro_tres: Informa os indicadores relativos ao Quadro 3
        da nota técnica
    :type quadro_tres: QuadroTres
    
    :param quadro_quatro: Informa os indicadores relativos ao Quadro 4
        da nota técnica
    :type quadro_quatro: QuadroQuatro
    
    :param quadro_cinco: Informa os indicadores relativos ao Quadro 5
        da nota técnica
    :type quadro_cinco: QuadroCinco
    
    :param quadro_seis: Informa os indicadores relativos ao Quadro 6
        da nota técnica
    :type quadro_seis: QuadroSeis
    
    """
    
    def __init__(self, codigo_nucleo, mes_referencia, quadro_um, quadro_dois, quadro_tres, quadro_quatro, quadro_cinco, quadro_seis):
        """Inicializa uma nova instância da classe com os indicadores passados
            nos parâmetros."""
        
        self.codigo_nucleo = codigo_nucleo
        self.mes_referencia = mes_referencia
        self.quadro_um = quadro_um
        self.quadro_dois = quadro_dois
        self.quadro_tres = quadro_tres
        self.quadro_quatro = quadro_quatro
        self.quadro_cinco = quadro_cinco
        self.quadro_seis = quadro_seis
        
class QuadroUm:
    """Classe com todas as funções necessárias para o recebimento do Quadro 1
        (Indicadores de estrutura para monitoramento e avaliação do Programa
        Nacional Telessaúde Brasil Redes).

        :param num_profissionais_qualificados: Numero de profissionais que
            foram qualificados para uso das ferramentas em Telessaúde
        :type num_profissionais_qualificados: int
        
        :param num_dispositivos_movel: Meio de acesso ao serviço de Telessaúde
            através de dispositivo móveis
        :type num_dispositivos_movel: int
        
        :param num_dispositivos_fixo: Meio de acesso ao serviço de Telessaúde
            através de dispositivo fixos
        :type num_dispositivos_fixo: int
        
    """
    
    def __init__(self, num_profissionais_qualificados, num_dispositivos_movel, num_dispositivos_fixo):
        self.num_profissionais_qualificados = num_profissionais_qualificados
        self.num_dispositivos_movel = num_dispositivos_movel
        self.num_dispositivos_fixo = num_dispositivos_fixo
        self.avaliacao_estrutura_municipio = []
        self.profissionais_registrados = []

    def add_avaliacao_estrutura(self, codigo_municipio, num_ubs_pontos_em_implantacao, num_ubs_pontos_implantados, num_equipe_saude_atendidas):
        """Adiciona indicadores (numPontosEmImplantacao, numPontosImplantados
            e numEquipesSaude) em cada município (codigo_municipio).

            :param codigo_municipio: Código IBGE do município
            :type codigo_municipio: str
            
            :param num_ubs_pontos_em_implantacao: Número de unidades de saúde
                com pontos em implantação de Telessaúde em cada município
            :type num_ubs_pontos_em_implantacao: int
            
            :param num_ubs_pontos_implantados: Número de unidades de saúde com
                pontos implantados de Telessaúde em cada município
            :type num_ubs_pontos_implantados: int
            
            :param num_equipe_saude_atendidas: Número de equipes de saúde
                atendidas por Telessaúde em cada município
            :type num_equipe_saude_atendidas: int
        
        """
        
        avaliacao_estrutura_municipio = {}
        avaliacao_estrutura_municipio['codigo_municipio'] = codigo_municipio
        avaliacao_estrutura_municipio['num_ubs_pontos_em_implantacao'] = num_ubs_pontos_em_implantacao
        avaliacao_estrutura_municipio['num_ubs_pontos_implantados'] = num_ubs_pontos_implantados
        avaliacao_estrutura_municipio['num_equipe_saude_atendidas'] = num_equipe_saude_atendidas
        
        self.avaliacao_estrutura_municipio.append(avaliacao_estrutura_municipio)

    def add_profissionais_registrados(self, codigo_municipio, codigo_familia_cbo, numero):
        """Adiciona o indicador "Número de profissionais registrados em cada
            município (codigo_municipio) e em cada categoria profissional
            (codigo_familia_cbo)"
        
            :param codigo_municipio: Código IBGE do município
            :type codigo_municipio: str
            
            :param codigo_familia_cbo: Código de 4 digítos referente à família
                ocupacional na CBO2002
            :type codigo_familia_cbo: str
            
            :param numero: Valor do indicador
            :type numero: int
        
        """
        
        profissionais_registrados = {}
        profissionais_registrados['codigo_municipio'] = codigo_municipio
        profissionais_registrados['codigo_familia_cbo'] = codigo_familia_cbo
        profissionais_registrados['numero'] = numero
        
        self.profissionais_registrados.append(profissionais_registrados)
        
class QuadroDois:
    """Classe com todas as funções necessárias para o recebimento do Quadro 2
        (Indicadores mínimos de processo para monitoramento
        e avaliação de Teleconsultoria)

        :param num_sincronas: Número de solicitações de teleconsultorias
            síncronas respondidas
        :type num_sincronas: int
        
        :param num_assincronas: Número de solicitações de teleconsultorias
            assíncronas respondidas
        :type num_assincronas: int
        
        :param num_pontos_ativos: Número de pontos ativos (pelo menos uma
            utilização de serviços mensal) em teleconsultorias
        :type num_pontos_ativos: int
        
        :param percentual_aprovado_cib: % de solicitações de teleconsultorias
            baseadas em protocolos de regulação aprovadas na CIB
        :type percentual_aprovado_cib: double
    
    """
    
    def __init__(self, num_sincronas, num_assincronas, num_pontos_ativos, percentual_aprovado_cib):
        self.num_sincronas = num_sincronas
        self.num_assincronas = num_assincronas
        self.num_pontos_ativos = num_pontos_ativos
        self.percentual_aprovado_cib = percentual_aprovado_cib
        self.solicitacoes_uf = []
        self.solicitacoes_municipio = []
        self.solicitacoes_equipe = []
        self.solicitacoes_membro = []
        self.solicitacoes_ponto = []
        self.solicitacoes_profissional = []
        self.solicitacoes_tema_profissional = []
        self.solicitacoes_cat_profissional = []
        
    def add_solicitacoes_uf(self, codigo_uf, numero):
        """Adiciona o indicador "Número de solicitações de teleconsultorias
            no estado (codigo_uf) respondidas"
        
            :param codigo_uf: Código IBGE do estado
            :type codigo_uf: str
            
            :param numero: Valor do indicador
            :type numero: int
            
        """
        
        solicitacoes_uf = {}
        solicitacoes_uf['codigo_uf'] = codigo_uf
        solicitacoes_uf['numero'] = numero
        
        self.solicitacoes_uf.append(solicitacoes_uf)

    def add_solicitacoes_municipio(self, codigo_municipio, numero):
        """Adiciona o indicador "Número de solicitações de teleconsultorias
            por município (codigo_municipio) respondidas"
        
            :param codigo_municipio: Código IBGE do município
            :type codigo_municipio: str
            
            :param numero: Valor do indicador
            :type numero: int
            
        """
        
        solicitacoes_municipio = {}
        solicitacoes_municipio['codigo_municipio'] = codigo_municipio
        solicitacoes_municipio['numero'] = numero
    
        self.solicitacoes_municipio.append(solicitacoes_municipio)

    def add_solicitacoes_equipe(self, codigo_equipe, codigo_equipe_ine, numero):
        """Adiciona o indicador "Número de solicitações de teleconsultorias
            por equipe de saúde respondidas"
        
            .. note::
            
                Pelo menos um dos dois (codigo_equipe ou codigo_equipe_ine)
                deve ser informado.
                
            :param codigo_equipe: Código de identificação de equipe de saúde
                utilizada pelo sistema de teleconsultoria
            :type codigo_equipe: str
            
            :param codigo_equipe_ine: Código Identificador Nacional de Equipe
                (INE)
            :type codigo_equipe_ine: str
            
            :param numero: Valor do indicador
            :type numero: int
            
        """
        
        solicitacoes_equipe = {}
        solicitacoes_equipe['codigo_equipe'] = codigo_equipe
        solicitacoes_equipe['codigo_equipe_ine'] = codigo_equipe_ine
        solicitacoes_equipe['numero'] = numero
    
        self.solicitacoes_equipe.append(solicitacoes_equipe)
    
    def add_solicitacoes_membro_gestao(self, codigo_cpf, codigo_cns, membro_nome, email, numero):
        """Adiciona o indicador "Número de solicitações de teleconsultorias
            por membro de gestão respondidas"
        
            .. note::
            
                A indentificação do membro de gestão será inicialmente pelo
                CPF (codigo_cpf), caso não exista usar o identificador Cartão
                Nacional de Saúde (CNS) e como última opção o email.
        
            :param codigo_cpf: CPF  do membro de gestão
            :type codigo_cpf: str
            
            :param codigo_cns: Identificador CNS do membro de gestão
            :type codigo_cns: str
            
            :param membro_nome: Nome do membro de gestão
            :type membro_nome: str
            
            :param email: Email do membro de gestão
            :type email: str
            
            :param numero: Valor do indicador
            :type numero: int
            
        """
        
        solicitacoes_membro = {}
        solicitacoes_membro['codigo_cpf'] = codigo_cpf
        solicitacoes_membro['codigo_cns'] = codigo_cns
        solicitacoes_membro['email'] = email
        solicitacoes_membro['membro_nome'] = membro_nome
        solicitacoes_membro['numero'] = numero
    
        self.solicitacoes_membro.append(solicitacoes_membro)

    def add_solicitacoes_ponto_telessaude(self, codigo_ponto, numero):
        """Adiciona o indicador "Número de solicitações de teleconsultorias
            por ponto (codigo_ponto) respondidas"
        
            :param codigo_ponto: Código Cadastro Nacional de
                Estabelecimento de Saúde (CNES)
            :type codigo_ponto: str
            
            :param numero: Valor do indicador
            :type numero: int
            
        """
        
        solicitacoes_ponto = {}
        solicitacoes_ponto['codigo_ponto'] = codigo_ponto
        solicitacoes_ponto['numero'] = numero
    
        self.solicitacoes_ponto.append(solicitacoes_ponto)
        
    def add_solicitacoes_profissional(self, profissional_cpf, profissional_codigo_cns, nome, email, codigo_tipo_profissional, codigo_cbo, codigo_equipe, codigo_equipe_ine, numero):
        """Adiciona o indicador "Número total de solicitações por
            profissional respondidadas"
        
            .. note::
            
                A indentificação do profissional de saúde será inicialmente
                pelo CPF (codigo_cpf), caso não exista usar o identificador
                Cartão Nacional de Saúde (CNS) e como última opção o email.
        
            :param profissional_cpf: CPF  do profissional
            :type profissional_cpf: str
            
            :param profissional_cpf: Identificador CNS do profissional
            :type profissional_cpf: str
            
            :param profissional_nome: Nome do profissional
            :type profissional_nome: str
            
            :param email: Email do profissional
            :type email: str
            
            :param codigo_tipo_profissional: Tipo do profissional
            :type codigo_tipo_profissional: str

            :param codigo_cbo: Código de 6 digítos referente à ocupação na CBO2002.
            :type codigo_cbo: str

            :param codigo_equipe: Código de identificação de equipe de saúde utilizada pelo sistema de teleconsultoria.
            :type codigo_equipe: str
            
            :param codigo_equipe_ine: Código Identificador Nacional de Equipe (INE).
            :type codigo_equipe_ine: str
            
            :param numero: Valor do indicador
            :type numero: int
            
        """
        
        solicitacoes_profissional = {}
        solicitacoes_profissional['profissional_cpf'] = profissional_cpf
        solicitacoes_profissional['profissional_codigo_cns'] = profissional_codigo_cns
        solicitacoes_profissional['profissional_email'] = email
        solicitacoes_profissional['nome'] = nome
        solicitacoes_profissional['codigo_tipo_profissional'] = codigo_tipo_profissional
        solicitacoes_profissional['codigo_cbo'] = codigo_cbo
        solicitacoes_profissional['codigo_equipe'] = codigo_equipe
        solicitacoes_profissional['codigo_equipe_ine'] = codigo_equipe_ine
        solicitacoes_profissional['numero'] = numero
    
        self.solicitacoes_profissional.append(solicitacoes_profissional)
        
    def add_solicitacoes_profissional_tema(self, profissional_cpf, profissional_codigo_cns, profissional_email, codigo_cid, codigo_ciap, numero):
        """Adiciona o indicador "Número total de solicitações por Profissional de saúde e por tema (CID e/ou CIAP2) respondidas".
        
            .. note::
            
                Observações: 
                    1. profissional_cpf, profissional_codigo_cns e profissional_email são utilizados para localizar o profissional de saúde, ao menos um dos três deve ser informado.
                    2. Ao menos codigo_cid ou codigo_ciap deve ser informado.
        
            :param profissional_cpf: CPF  do profissional
            :type profissional_cpf: str
            
            :param profissional_cpf: Identificador CNS do profissional
            :type profissional_cpf: str
            
            :param email: Email do profissional
            :type email: str
            
            :param codigo_cid: Código CID (Classificação Internacional de Doenças).
            :type codigo_cid: str
            
            :param codigo_ciap: Código CIAP (Classificação Internacional de Assistência Primária).
            :type codigo_ciap: str

            :param numero: Valor do indicador
            :type numero: int
        """
        
        solicitacoes_tema_profissional = {}
        
        solicitacoes_tema_profissional['profissional_cpf'] = profissional_cpf
        solicitacoes_tema_profissional['profissional_codigo_cns'] = profissional_codigo_cns
        solicitacoes_tema_profissional['profissional_email'] = profissional_email
        solicitacoes_tema_profissional['codigo_cid'] = codigo_cid
        solicitacoes_tema_profissional['codigo_ciap'] = codigo_ciap
        solicitacoes_tema_profissional['numero'] = numero
        
        self.solicitacoes_tema_profissional.append(solicitacoes_tema_profissional)
        
    def add_solicitacoes_cat_profissional(self, codigo_familia_cbo, numero):
        """Adiciona o indicador "Número de solicitações de teleconsultorias
            por categoria profissional respondidas"
        
            :param codigo_familia_cbo: Código de 4 digítos referente
                à família ocupacional na CBO2002
            :type codigo_familia_cbo: str
            
            :param numero: Valor do indicador
            :type numero: int
            
        """
        
        solicitacoes_cat_profissional = {}
        solicitacoes_cat_profissional['codigo_familia_cbo'] = codigo_familia_cbo
        solicitacoes_cat_profissional['numero'] = numero
    
        self.solicitacoes_cat_profissional.append(solicitacoes_cat_profissional)
        
class QuadroTres:
    """Classe com todas as funções necessárias para o recebimento do Quadro 3
        (Indicadores mínimos de processo para monitoramento
        e avaliação de Telediagnóstico)

        :param num_pontos_ativos: Nº de pontos ativos (pelo menos
            uma utilização de serviço mensal) em telediagóstico
        :type num_pontos_ativos: int
        
        :param percentual_aprovado_cib: % de solicitações de telediagnósticos
            baseadas em protocolos de regulação aprovadas na CIB
        :type percentual_aprovado_cib: double
    
    """
    
    def __init__(self, num_pontos_ativos, porcentual_aprovado_cib):
        self.num_pontos_ativos = num_pontos_ativos
        self.porcentual_aprovado_cib = porcentual_aprovado_cib
        self.solicitacoes_telediagnostico_uf = []
        self.solicitacoes_telediagnostico_municipio = []
        self.solicitacoes_telediagnostico_equipe = []
        self.solicitacoes_telediagnostico_ponto = []
        self.solicitacoes_telediagnostico_tipo = []

    def add_solicitacoes_telediagnostico_uf(self, codigo_uf, numero):
        """Adiciona o indicador "Número de solicitações com exame realizado
            e laudo enviado ao solicitado por estado (codigo_uf)"
            
            :param codigo_uf: Código IBGE do estado
            :type codigo_uf: str
        
            :param numero: Valor do indicador
            :type numero: int
            
        """
        
        solicitacoes_telediagnostico_uf = {}
        solicitacoes_telediagnostico_uf['codigo_uf'] = codigo_uf
        solicitacoes_telediagnostico_uf['numero'] = numero
        
        self.solicitacoes_telediagnostico_uf.append(solicitacoes_telediagnostico_uf)

    def add_solicitacoes_telediagnostico_municipio(self, codigo_municipio, numero):
        """Adiciona o indicador "Número de solicitações com exame realizado
            e laudo enviado ao solicitado por município (codigo_municipio)"
            
            :param codigo_municipio: Código IBGE do município
            :type codigo_municipio: str
        
            :param numero: Valor do indicador
            :type numero: int
            
        """
        
        solicitacoes_telediagnostico_municipio = {}
        solicitacoes_telediagnostico_municipio['codigo_municipio'] = codigo_municipio
        solicitacoes_telediagnostico_municipio['numero'] = numero
        
        self.solicitacoes_telediagnostico_municipio.append(solicitacoes_telediagnostico_municipio)

    def add_solicitacoes_telediagnostico_ponto_telessaude(self, codigo_ponto, numero):
        """Adiciona o indicador "Número de solicitações com exame realizado
            e laudo enviado ao solicitante por ponto de telessaúde"
            
            :param codigo_ponto: Código Cadastro Nacional de Estabelecimento
                de Saúde (CNES)
            :type codigo_ponto: str
        
            :param numero: Valor do indicador
            :type numero: int
            
        """
        
        solicitacoes_telediagnostico_ponto = {}
        solicitacoes_telediagnostico_ponto['codigo_ponto'] = codigo_ponto
        solicitacoes_telediagnostico_ponto['numero'] = numero
        
        self.solicitacoes_telediagnostico_ponto.append(solicitacoes_telediagnostico_ponto)

    def add_solicitacoes_telediagnostico_equipe(self, codigo_equipe, codigo_equipe_ine, numero):
        """Adiciona o indicador "Número de solicitações com exame realizado
            com exame realizado e laudo enviado ao solicitante por equipe"
        
            .. note::
            
                Pelo menos um dos dois (codigo_equipe ou codigo_equipe_ine)
                deve ser informado.
                
            :param codigo_equipe: Código de identificação de equipe de saúde
                utilizada pelo sistema de teleconsultoria
            :type codigo_equipe: str
            
            :param codigo_equipe_ine: Código Identificador Nacional de Equipe
                (INE)
            :type codigo_equipe_ine: str
            
            :param numero: Valor do indicador
            :type numero: int
            
        """
        
        solicitacoes_telediagnostico_equipe = {}
        solicitacoes_telediagnostico_equipe['codigo_equipe'] = codigo_equipe
        solicitacoes_telediagnostico_equipe['codigo_equipe_ine'] = codigo_equipe_ine
        solicitacoes_telediagnostico_equipe['numero'] = numero
        
        self.solicitacoes_telediagnostico_equipe.append(solicitacoes_telediagnostico_equipe)
    
    def add_solicitacoes_telediagnostico_tipo(self, codigo_sia, numero):
        """Número de solicitações com exame realizado e laudo enviado ao solicitante por tipo (codigoSIA).
            
            :param codigo_sia
            :type codigo_sia 
            
            :param numero
            :type numero 
        """
        solicitacoes_telediagnostico_tipo = {}
        solicitacoes_telediagnostico_tipo['codigo_sia'] = codigo_sia
        solicitacoes_telediagnostico_tipo['numero'] = numero
        self.solicitacoes_telediagnostico_tipo.append(solicitacoes_telediagnostico_tipo)
        
class QuadroQuatro:
    """Classe com todas as funções necessárias para o recebimento do Quadro 4
        (Indicadores mínimos de processo para monitoramento
        e avaliação de Tele-educação)

        :param quantidade_disponibilizada_ares: Número de objetos de
            aprendizagem disponibilizados no ARES por mês
        :type quantidade_disponibilizada_ares: int
        
        :param num_pontos_ativos: Número de pontos ativos (pelo menos uma
            utilização de serviços mensal) em tele-educação
        :type num_pontos_ativos: int
    
    """
    
    def __init__(self, quantidade_disponibilizada_ares, num_pontos_ativos):
        self.quantidade_disponibilizada_ares = quantidade_disponibilizada_ares
        self.num_pontos_ativos = num_pontos_ativos
        self.atividades_realizadas_uf = []
        self.atividades_realizadas_municipio = []
        self.atividades_realizadas_ponto = []
        self.atividades_realizadas_equipe = []
        self.participantes_cat_profissional_uf = []
        self.participantes_cat_profissional_municipio = []
        self.participantes_cat_profissional_equipe = []
        self.participantes_cat_profissional_ponto = []
        self.acessos_objetos_aprendizagem = []
        self.acessos_objetos_aprendizagem_municipio = []
        self.acessos_objetos_aprendizagem_equipe = []
        self.acessos_objetos_aprendizagem_ponto = []
        self.acessos_objetos_aprendizagem_cat_profissional = []
    
    def add_atividades_realizadas_uf(self, codigo_uf, numero):
        """Adiciona o indicador "Número de atividades realizadas
            por estado (codigo_uf)"
            
            :param codigo_uf: Código IBGE do estado
            :type codigo_uf: str
        
            :param numero: Valor do indicador
            :type numero: int
            
        """
        
        atividades_realizadas_uf = {}
        atividades_realizadas_uf['codigo_uf'] = codigo_uf
        atividades_realizadas_uf['numero'] = numero
        
        self.atividades_realizadas_uf.append(atividades_realizadas_uf)
    
    def add_atividades_realizadas_municipio(self, codigo_municipio, numero):
        """Adiciona o indicador "Número de atividades realizadas
            por município (codigo_municipio)"
            
            :param codigo_municipio: Código IBGE do município
            :type codigo_municipio: str
            
            :param numero: Valor do indicador
            :type numero: int
            
        """
        
        atividades_realizadas_municipio = {}
        atividades_realizadas_municipio['codigo_municipio'] = codigo_municipio
        atividades_realizadas_municipio['numero'] = numero
        
        self.atividades_realizadas_municipio.append(atividades_realizadas_municipio)
    
    def add_atividades_realizadas_ponto_telessaude(self, codigo_ponto, numero):
        """Adiciona o indicador "Número de atividades realizadas
            por ponto de telessaúde (codigo_ponto)"
            
            :param codigo_ponto: Código Cadastro Nacional de Estabelecimento de Saúde (CNES)
            :type codigo_ponto: str
            
            :param numero: Valor do indicador
            :type numero: int
            
        """
        
        atividades_realizadas_ponto = {}
        atividades_realizadas_ponto['codigo_ponto'] = codigo_ponto
        atividades_realizadas_ponto['numero'] = numero
        
        self.atividades_realizadas_ponto.append(atividades_realizadas_ponto)
    
    def add_atividades_realizadas_equipe(self, codigo_equipe, codigo_equipe_ine, numero):
        """Adiciona o indicador "Número de atividades realizdas por equipe"
        
            .. note::
            
                Pelo menos um dos dois (codigo_equipe ou codigo_equipe_ine)
                deve ser informado.
                
            :param codigo_equipe: Código de identificação de equipe de saúde
                utilizada pelo sistema de teleconsultoria
            :type codigo_equipe: str
            
            :param codigo_equipe_ine: Código Identificador Nacional de Equipe
                (INE)
            :type codigo_equipe_ine: str
            
            :param numero: Valor do indicador
            :type numero: int
            
        """
        
        atividades_realizadas_equipe = {}
        atividades_realizadas_equipe['codigo_equipe'] = codigo_equipe
        atividades_realizadas_equipe['codigo_equipe_ine'] = codigo_equipe_ine
        atividades_realizadas_equipe['numero'] = numero
        
        self.atividades_realizadas_equipe.append(atividades_realizadas_equipe)
    
    def add_participantes_cat_profissional_uf(self, codigo_uf, codigo_familia_cbo, numero):
        """Adiciona o indicador "Número de atividades realizadas
            por categoriga profissional (codigo_familia_cbo)
            por estado (codigo_uf)"
            
            :param codigo_uf: Código IBGE do estado
            :type codigo_uf: str
            
            :param codigo_familia_cbo: Código de 4 digítos referente
                à família ocupacional na CBO2002
            :type codigo_familia_cbo: str
            
            :param numero: Valor do indicador
            :type numero: int
            
        """
        
        participantes_cat_profissional_uf = {}
        participantes_cat_profissional_uf['codigo_uf'] = codigo_uf
        participantes_cat_profissional_uf['codigo_familia_cbo'] = codigo_familia_cbo
        participantes_cat_profissional_uf['numero'] = numero
        
        self.participantes_cat_profissional_uf.append(participantes_cat_profissional_uf)
    
    def add_participantes_cat_profissional_municipio(self, codigo_municipio, codigo_familia_cbo, numero):
        """Adiciona o indicador "Número de atividades realizadas
            por categoriga profissional (codigo_familia_cbo)
            por município (codigo_municipio)"
            
            :param codigo_municipio: Código IBGE do município
            :type codigo_municipio: str
            
            :param codigo_familia_cbo: Código de 4 digítos referente
                à família ocupacional na CBO2002
            :type codigo_familia_cbo: str
            
            :param numero: Valor do indicador
            :type numero: int
            
        """
        
        participantes_cat_profissional_municipio = {}
        participantes_cat_profissional_municipio['codigo_municipio'] = codigo_municipio
        participantes_cat_profissional_municipio['codigo_familia_cbo'] = codigo_familia_cbo
        participantes_cat_profissional_municipio['numero'] = numero
        
        self.participantes_cat_profissional_municipio.append(participantes_cat_profissional_municipio)
    
    def add_participantes_cat_profissional_equipe(self, codigo_equipe, codigo_equipe_ine, codigo_familia_cbo, numero):
        """Adiciona o indicador "Número de participantes por categoriga
            profissional (codigo_familia_cbo) por equipe"
        
            .. note::
            
                Pelo menos um dos dois (codigo_equipe ou codigo_equipe_ine)
                deve ser informado.
                
            :param codigo_equipe: Código de identificação de equipe de saúde
                utilizada pelo sistema de teleconsultoria
            :type codigo_equipe: str
            
            :param codigo_equipe_ine: Código Identificador Nacional de Equipe
                (INE)
            :type codigo_equipe_ine: str
            
            :param codigo_familia_cbo: Código de 4 digítos referente
                à família ocupacional na CBO2002
            :type codigo_familia_cbo: str
            
            :param numero: Valor do indicador
            :type numero: int
            
        """
        
        participantes_cat_profissional_equipe = {}
        participantes_cat_profissional_equipe['codigo_equipe'] = codigo_equipe
        participantes_cat_profissional_equipe['codigo_equipe_ine'] = codigo_equipe_ine
        participantes_cat_profissional_equipe['codigo_familia_cbo'] = codigo_familia_cbo
        participantes_cat_profissional_equipe['numero'] = numero
        
        self.participantes_cat_profissional_equipe.append(participantes_cat_profissional_equipe)
    
    def add_participantes_cat_profissional_ponto_telessaude(self, codigo_ponto, codigo_familia_cbo, numero):
        """Adiciona o indicador "Número de atividades realizadas
            por categoriga profissional (codigo_familia_cbo)
            por por ponto/mês (codigo_ponto)"
            
            :param codigo_ponto: Código Cadastro Nacional
                de Estabelecimento de Saúde (CNES)
            :type codigo_ponto: str
            
            :param codigo_familia_cbo: Código de 4 digítos referente
                à família ocupacional na CBO2002
            :type codigo_familia_cbo: str
            
            :param numero: Valor do indicador
            :type numero: int
            
        """
        
        participantes_cat_profissional_ponto = {}
        participantes_cat_profissional_ponto['codigo_ponto'] = codigo_ponto
        participantes_cat_profissional_ponto['codigo_familia_cbo'] = codigo_familia_cbo
        participantes_cat_profissional_ponto['numero'] = numero
        
        self.participantes_cat_profissional_ponto.append(participantes_cat_profissional_ponto)
    
    def add_acessos_objetos_aprendizagem(self, codigo_uf, codigo_municipio, codigo_equipe, codigo_equipe_ine, codigo_ponto, numero):
        """Adiciona o indicador "Número global de acessos aos objetos
            de aprendizagem por estado, município, equipe e ponto/mês"
        
            .. note::
            
                Pelo menos um dos dois (codigo_equipe ou codigo_equipe_ine)
                deve ser informado.
            
            :param codigo_uf: Código IBGE do estado
            :type codigo_uf: str
            
            :param codigo_municipio: Código IBGE do município
            :type codigo_municipio: str
            
            :param codigo_equipe: Código de identificação de equipe de saúde
                utilizada pelo sistema de teleconsultoria
            :type codigo_equipe: str
            
            :param codigo_equipe_ine: Código Identificador Nacional de Equipe
                (INE)
            :type codigo_equipe_ine: str
            
            :param codigo_ponto: Código Cadastro Nacional de Estabelecimento
                de Saúde (CNES)
            :type codigo_ponto: str
            
            :param numero: Valor do indicador
            :type numero: int
            
        """
        
        acessos_objetos_aprendizagem = {}
        acessos_objetos_aprendizagem['codigo_uf'] = codigo_uf
        acessos_objetos_aprendizagem['codigo_municipio'] = codigo_municipio
        acessos_objetos_aprendizagem['codigo_equipe'] = codigo_equipe
        acessos_objetos_aprendizagem['codigo_equipe_ine'] = codigo_equipe_ine
        acessos_objetos_aprendizagem['codigo_ponto'] = codigo_ponto
        acessos_objetos_aprendizagem['numero'] = numero
        
        self.acessos_objetos_aprendizagem.append(acessos_objetos_aprendizagem)
    
    def add_acessos_objetos_aprendizagem_municipio(self, codigo_municipio, numero):
        """Adiciona o indicador "Número global de acessos aos objetos
            de aprendizagem por município (codigo_municipio)"
            
            :param codigo_municipio: Código IBGE do município
            :type codigo_municipio: str
            
            :param numero: Valor do indicador
            :type numero: int
            
        """
        
        acessos_objetos_aprendizagem_municipio = {}
        acessos_objetos_aprendizagem_municipio['codigo_municipio'] = codigo_municipio
        acessos_objetos_aprendizagem_municipio['numero'] = numero
        
        self.acessos_objetos_aprendizagem_municipio.append(acessos_objetos_aprendizagem_municipio)
    
    def add_acessos_objetos_aprendizagem_ponto_telessaude(self, codigo_ponto, numero):
        """Adiciona o indicador "Número global de acessos aos objetos
            de aprendizagem por ponto de telessaúde"
            
            :param codigo_ponto: Código IBGE do município
            :type codigo_ponto: str
            
            :param numero: Valor do indicador
            :type numero: int
            
        """
        
        acessos_objetos_aprendizagem_ponto = {}
        acessos_objetos_aprendizagem_ponto['codigo_ponto'] = codigo_ponto
        acessos_objetos_aprendizagem_ponto['numero'] = numero
        
        self.acessos_objetos_aprendizagem_ponto.append(acessos_objetos_aprendizagem_ponto)
        
    def add_acessos_objetos_aprendizagem_equipe(self, codigo_equipe, codigo_equipe_ine, numero):
        """Adiciona o indicador "Número global de acessos aos objetos
            de aprendizagem por equipe"
        
            .. note::
            
                Pelo menos um dos dois (codigo_equipe ou codigo_equipe_ine)
                deve ser informado.
                
            :param codigo_equipe: Código de identificação de equipe de saúde
                utilizada pelo sistema de teleconsultoria
            :type codigo_equipe: str
            
            :param codigo_equipe_ine: Código Identificador Nacional de Equipe
                (INE)
            :type codigo_equipe_ine: str
            
            :param numero: Valor do indicador
            :type numero: int
            
        """
        
        acessos_objetos_aprendizagem_equipe = {}
        acessos_objetos_aprendizagem_equipe['codigo_equipe'] = codigo_equipe
        acessos_objetos_aprendizagem_equipe['codigo_equipe_ine'] = codigo_equipe_ine
        acessos_objetos_aprendizagem_equipe['numero'] = numero
        
        self.acessos_objetos_aprendizagem_equipe.append(acessos_objetos_aprendizagem_equipe)
    
    def add_acessos_objetos_aprendizagem_cat_profissional(self, codigo_familia_cbo, numero):
        """Adiciona o indicador "Número global de acessos aos objetos
            de aprendizagem por categoria profissional (codigo_familia_cbo)"
            
            :param codigo_familia_cbo: Código de 4 digítos referente
                à família ocupacional na CBO2002
            :type codigo_familia_cbo: str
            
            :param numero: Valor do indicador
            :type numero: int
            
        """
        
        acessos_objetos_aprendizagem_cat_profissional = {}
        acessos_objetos_aprendizagem_cat_profissional['codigo_familia_cbo'] = codigo_familia_cbo
        acessos_objetos_aprendizagem_cat_profissional['numero'] = numero
        
        self.acessos_objetos_aprendizagem_cat_profissional.append(acessos_objetos_aprendizagem_cat_profissional)
        
class QuadroCinco:
    """Classe com todas as funções necessárias para o recebimento do Quadro 6
        (Indicadores mínimos de resultados e avaliação
        para monitoramento de Teleconsultoria)
        
        :param num_sof_enviada_bireme: Nº de SOF produzidas,
            enviadas e aprovadas pela BIREME
        :type num_sof_enviada_bireme: int
        
        :param tempo_medio_sincronas:
            Tempo médio de respostas de solicitações síncronas
        :type tempo_medio_sincronas: int
        
        :param tempo_medio_assincronas:
            Tempo médio de respostas de solicitações assíncronas
        :type tempo_medio_assincronas: int
        
        :param percentual_assinc_resp_emmenos72:
            % de solictações assíncronas respondidas em menos de 72h
        :type percentual_assinc_resp_emmenos72: double
        
    """
    
    def __init__(self, num_sof_enviada_bireme, tempo_medio_sincronas, tempo_medio_assincronas, percentual_assinc_resp_emmenos72):
        self.num_sof_enviada_bireme = num_sof_enviada_bireme
        self.tempo_medio_sincronas = tempo_medio_sincronas
        self.tempo_medio_assincronas = tempo_medio_assincronas
        self.percentual_assinc_resp_emmenos72 = percentual_assinc_resp_emmenos72
        self.satisfacao_solicitante = []
        self.resolucao_duvida = []
        self.temas_frequentes = []
        self.cat_profissionais_frequentes = []
        self.especialidades_frequentes = []
        self.evitacao_encaminhamentos = []
    
    def add_temas_frequentes(self, codigo_cid, codigo_ciap):
        """Adiciona o indicador "Lista dos 10 temas mais frequentes
            das solicitações de teleconsultorias respondidas"
            
            :param codigo_cid: Código CID
                (Classificação Internacional de Doenças)
            :type codigo_cid: str
            
            :param codigo_ciap: Código CIAP
                (Classificação Internacional de Assistência Primária)
            :type codigo_ciap: str
            
        """
        
        temas_frequentes = {}
        temas_frequentes['codigo_cid'] = codigo_cid
        temas_frequentes['codigo_ciap'] = codigo_ciap
        
        self.temas_frequentes.append(temas_frequentes)
    
    def add_especialidades_frequentes(self, codigo_cbo):
        """Adiciona o indicador "Especialidades dos teleconsultores
            mais frequentes entre as solicitações de telconsultorias respondidas"
            
            :param codigo_cbo: Código de 6 digítos referente
                à ocupação na CBO2002
            :type codigo_cbo: str
            
        """
        
        especialidades_frequentes = {}
        especialidades_frequentes['codigo_cbo'] = codigo_cbo
        
        self.especialidades_frequentes.append(especialidades_frequentes)
    
    
    def add_cat_profissionais_frequentes(self, codigo_familia_cbo):
        """Adiciona o indicador "Categoria profissional dos teleconsultores
            mais frequentes entre as solicitações
            de telconsultorias respondidas"
            
            :param codigo_familia_cbo: Código de 4 digítos referente
                à família ocupacional na CBO2002
            :type codigo_familia_cbo: str
            
        """
        
        cat_profissionais_frequentes = {}
        cat_profissionais_frequentes['codigo_familia_cbo'] = codigo_familia_cbo
        
        self.cat_profissionais_frequentes.append(cat_profissionais_frequentes)

    def add_satisfacao_solicitante(self, codigo_escala_likert, percentual):
        """Adiciona o indicador "Percentual de teleconsultorias respondidas
            em que houve satisfação do solicitante"
            
            :param codigo_escala_likert: Código na Escala Likert
                (consulta código no sistema)
            :type codigo_escala_likert: str
            
            :param percentual: Valor do indicador
            :type percentual: int
            
        """
        
        satisfacao_solicitante = {}
        satisfacao_solicitante['codigo_escala_likert'] = codigo_escala_likert
        satisfacao_solicitante['percentual'] = percentual
        
        self.satisfacao_solicitante.append(satisfacao_solicitante)

    def add_resolucao_duvida(self, percentual_sim, percentual_parcial, percentual_nao, percentual_nao_sei):
        """Adiciona o indicador "Percentual de teleconsultorias respondidas
            em que houve resolução da dúvida (sim, parcialmente, não, não sei"
            
            :param percentual_sim: Valor do percentual para "Sim"
            :type percentual_sim: double
            
            :param percentual_parcial: Valor do percentual para "Parcialmente"
            :type percentual_parcial: double
            
            :param percentual_nao: Valor do percentual para "Não"
            :type percentual_nao: double
            
            :param percentual_nao_sei:
                Valor do percentual para "Não Sei/Não avaliado"
            :type percentual_nao_sei: double
            
        """
        
        resolucao_duvida = {}
        resolucao_duvida['percentual_sim'] = percentual_sim
        resolucao_duvida['percentual_parcial'] = percentual_parcial
        resolucao_duvida['percentual_nao'] = percentual_nao
        resolucao_duvida['percentual_nao_sei'] = percentual_nao_sei
        
        self.resolucao_duvida.append(resolucao_duvida)
    
    def add_evitacao_encaminhamento_cat_profissional(self, codigo_cbo, percentual_sim, percentual_nao):
        """ Adiciona o indicador "% teleconsultorias respondidas que havia intenção 
            de encaminhar paciente em que houve evitação de encaminhamentos"
            
            :param codigo_cbo: Código de 6 digítos referente
                à ocupação na CBO2002
            :type codigo_cbo: str
            
            :param percentual_sim: Percentual de Sim
                à ocupação na CBO2002
            :type percentual_sim: str
            
            :param percentual_nao: Percentual de Não
                à ocupação na CBO2002
            :type percentual_nao: str
        
        """
        evitacao_encaminhamento = {}
        evitacao_encaminhamento['codigo_familia_cbo'] = codigo_cbo
        evitacao_encaminhamento['percentual_sim'] = percentual_sim
        evitacao_encaminhamento['percentual_nao'] = percentual_nao
        
        self.evitacao_encaminhamentos.append(evitacao_encaminhamento)
        
class QuadroSeis:
    """Classe com todas as funções necessárias para o recebimento do Quadro 6
        (Indicadores de resultados e avaliação para a Tele-educação)
    
    """
    
    def __init__(self):
        self.temas_frequentes_participacao = []
        self.avaliacao_satisfacao_participantes = []
        self.temas_frequntes_objeto_aprendizagem = []
        self.avaliacao_satisfacao_objeto_aprendizagem = []

    def add_temas_frequentes_participacao(self, codigo_decs_bireme):
        """Adiciona o indicador "Até 5 temas com maior participação por mês"
            
            :param codigo_decs_bireme: Código do DeCS
                (Descritores em Ciências da Saúde) da BIREME
            :type codigo_decs_bireme: str
            
        """
        
        temas_frequentes_participacao = {}
        temas_frequentes_participacao['codigo_decs_bireme'] = codigo_decs_bireme
            
        self.temas_frequentes_participacao.append(temas_frequentes_participacao)

    def add_temas_frequentes_objeto_aprendizagem(self, codigo_decs_bireme):
        """Adiciona o indicador "Até 5 temas mais acessados,
            por objetos de aprendizagem"
            
            :param codigo_decs_bireme: Código do DeCS
                (Descritores em Ciências da Saúde) da BIREME
            :type codigo_decs_bireme: str
            
        """
        
        temas_frequntes_objeto_aprendizagem = {}
        temas_frequntes_objeto_aprendizagem['codigo_decs_bireme'] = codigo_decs_bireme
            
        self.temas_frequntes_objeto_aprendizagem.append(temas_frequntes_objeto_aprendizagem)

    def add_avaliacao_satisfacao_participantes(self, codigo_escala_likert, percentual):
        """Adiciona o indicador "Avaliação global da satisfação
            dos profissionais participantes do mês"
            
            :param codigo_escala_likert: Código na Escala Likert
                (consulta código no sistema)
            :type codigo_escala_likert: str
            
            :param percentual: Valor do indicador
            :type percentual: double
            
        """
        
        avaliacao_satisfacao_participantes = {}
        avaliacao_satisfacao_participantes['codigo_escala_likert'] = codigo_escala_likert
        avaliacao_satisfacao_participantes['percentual'] = percentual
            
        self.avaliacao_satisfacao_participantes.append(avaliacao_satisfacao_participantes)

    def add_avaliacao_satisfacao_objeto_aprendizagem(self, codigo_escala_likert, percentual):
        """Adiciona o indicador "Avaliação global da satisfação profissional
            com os objetos de aprendizagem por mês"
            
            :param codigo_escala_likert: Código na Escala Likert
                (consulta código no sistema)
            :type codigo_escala_likert: str
            
            :param percentual: Valor do indicador
            :type percentual: double
            
        """
        
        avaliacao_satisfacao_objeto_aprendizagem = {}
        avaliacao_satisfacao_objeto_aprendizagem['codigo_escala_likert'] = codigo_escala_likert
        avaliacao_satisfacao_objeto_aprendizagem['percentual'] = percentual
            
        self.avaliacao_satisfacao_objeto_aprendizagem.append(avaliacao_satisfacao_objeto_aprendizagem)
