Cliente REST do INTEGRA
=============

Instalação
-------------
Execute:

    ../integra-rest-python# python setup.py install

Distribuição
-------------
    ../integra-rest-python$ python setup.py sdist

Um arquivo compresso será gerado em "./dist".

O usuário deverá extrair seu conteúdo e executar:

    ../Integra-x.x.x# python setup.py install