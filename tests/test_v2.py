# -*- coding: utf-8 -*-
from integra import *
from integra.indicadores import *
from integra.constantes import *


def main():

    """ Para rodar os testes faz-se necessário pegar o token de acesso, para tal, acesse a "Visualização dos dados do Núcleo" no SMART.

        :param args
        :type args: str

    """
    integra = Integra('9723f53168b7cafbc4c9d7a95b3233fff366aad1')
    URL = 'http://localhost:8001/'
    # URL = 'http://smartteste.telessaude.ufrn.br/'

    print '\n\n////////////////// Profissionais //////////////////\n'
    profissionais(integra, URL)

    print '\n\n////////////////// Estabelecimentos //////////////////\n'
    estabelecimentos(integra, URL)

    print '\n////////////////// Teleconsultorias //////////////////\n'
    teleconsultorias(integra, URL)

    print '\n////////////////// Telediagnósticos //////////////////\n'
    telediagnosticos(integra, URL)


    print '\n\n////////////////// Atividades de Tele-educação //////////////////\n'
    atividades_teleeducacao(integra, URL)

    print '\n\n////////////////// Objetos de Aprendizagem //////////////////\n'
    objetos_aprendizagem(integra, URL)

    print '\n\n////////////////// Cursos //////////////////\n'
    cursos(integra, URL)

def profissionais(integra, URL):
    """ Simula o envio dos dados cadastrais de profissional de saúde.
        Nesse exemplos é enviado os dados cadastrais da profissional Ana Carolina Wanderley

        :param integra: Objeto integra, responsável por serializar os dados e enviá-los
        :type integra: Integra
        :param URL: O endereço do servidor
        :type URL: str

    """
    profissional = ProfissionalSaude("0000010", "022011")
    profissional.addProfissionalSaude("980016284186253", "01154825477", "Ana Carolina Wanderley Filgueiras", "2399741", "225135", "1", "01", Sexo.FEMININO)

    dados_serializados = Integra.serializar(profissional)
    print dados_serializados

    respostas = str(integra.enviar_dados(URL + 'api/v2/profissionais-saude/?format=json', dados_serializados))
    print respostas

def estabelecimentos(integra, URL):
    """ Simula o envio de atualização de estabelecimento de saúde.
        Nesse exemplo é atualizado os dados de quatro estabelecimentos.

        :param integra: Objeto integra, responsável por serializar os dados e enviá-los
        :type integra: Integra
        :param URL: O endereço do servidor
        :type URL: str

    """
    estabelecimento = EstabelecimentoSaude("0000010", "022011");
    estabelecimento.atualizarEstabelecimentoSaude("2653982", True, True, True);
    estabelecimento.atualizarEstabelecimentoSaude("2398419", False, False, True);
    estabelecimento.atualizarEstabelecimentoSaude("2653966", True, False, True);
    estabelecimento.atualizarEstabelecimentoSaude("2408627", False, False, False);


    dados_serializados = Integra.serializar(estabelecimento)
    print dados_serializados

    respostas = str(integra.enviar_dados(URL + 'api/v2/dados-estabelecimentos-saude/?format=json', dados_serializados))
    print respostas

def teleconsultorias(integra, URL):

    """ Simula o envio da produção de teleconsultoria.
        Nesse exemplo é enviado os dados de cinco teleconsultoria.

        :param integra: Objeto integra, responsável por serializar os dados e enviá-los
        :type integra: Integra
        :param URL: O endereço do servidor
        :type URL: str

    """
    teleconsultoria = Teleconsultoria("0000010", "022011")
    teleconsultoria.addTeleconsultoria(
        "05/01/2016 18:00:00",
        TipoTeleconsultoria.SINCRONA, CanalAcesso.TELEFONE,
        "03375447434", "225133", "2653982", None,  "01",
        ["a010", "a040", "w25"], ["R05", "A03", "R21"],
        "05/01/2016 18:20:00",
        EvitouEncaminhamentoTeleconsultoria.NAO, IntencaoEncaminhamentoTeleconsultoria.NAO,
        GrauSatisfacao.MUITO_SATISFEITO, ResolucaoDuvidaTeleconsultoria.ATENDEU_TOTALMENTE, False
    )

    teleconsultoria.addTeleconsultoria(
        "04/01/2016 18:00:00",
        TipoTeleconsultoria.SINCRONA, CanalAcesso.INTERNET,
        "49206079468", "322230", "2653982", None,  "01",
        ["a010", "a040", "w25"],
        None,
        "04/01/2016 18:20:00",
        EvitouEncaminhamentoTeleconsultoria.NAO, IntencaoEncaminhamentoTeleconsultoria.NAO,
        GrauSatisfacao.MUITO_INSATISFEITO, ResolucaoDuvidaTeleconsultoria.ATENDEU_TOTALMENTE, False
    )

    teleconsultoria.addTeleconsultoria(
        "03/01/2016 18:00:00",
        TipoTeleconsultoria.ASSINCRONA, CanalAcesso.INTERNET,
        "04390324403", "225225", "2653923", None, "01",
        ["a010", "a040", "w25"],
        None,
        "04/01/2016 18:00:00",
        EvitouEncaminhamentoTeleconsultoria.NAO, IntencaoEncaminhamentoTeleconsultoria.SIM,
        GrauSatisfacao.MUITO_INSATISFEITO, ResolucaoDuvidaTeleconsultoria.ATENDEU_TOTALMENTE, True
    )

    teleconsultoria.addTeleconsultoria(
        "02/01/2016 18:00:00",
        TipoTeleconsultoria.ASSINCRONA, CanalAcesso.INTERNET,
        "01097944433", "322205", "2653982", None, "01",
        None,
        ["R05", "A03", "R21"],
        "03/01/2016 18:00:00",
        EvitouEncaminhamentoTeleconsultoria.NAO_INFORMADO, IntencaoEncaminhamentoTeleconsultoria.NAO_INFORMADO,
        GrauSatisfacao.INSATISFEITO, ResolucaoDuvidaTeleconsultoria.NAO_ATENDEU, False
    )

    teleconsultoria.addTeleconsultoria(
        "01/01/2016 18:00:00",
        TipoTeleconsultoria.ASSINCRONA, CanalAcesso.INTERNET,
        "24609455072", "225203", "3649563", None, "01",
        ["a010", "a040", "w25"],
        ["R05", "A03", "R21"],
        "02/01/2016 18:00:00",
        EvitouEncaminhamentoTeleconsultoria.SIM, IntencaoEncaminhamentoTeleconsultoria.SIM,
        GrauSatisfacao.INDIFERENTE, ResolucaoDuvidaTeleconsultoria.ATENDEU_PARCIALMENTE, False
    )

    dados_serializados = Integra.serializar(teleconsultoria)
    print dados_serializados

    respostas = str(integra.enviar_dados(URL + 'api/v2/teleconsultorias/?format=json', dados_serializados))
    print respostas

def telediagnosticos(integra, URL):
    
    """ Simula o envio da produção de telediagnóstico.
        Nesse exemplo é enviado os dados de um telediagnóstico.

        :param integra: Objeto integra, responsável por serializar os dados e enviá-los
        :type integra: Integra
        :param URL: O endereço do servidor
        :type URL: str

    """
    telediagnostico = Telediagnostico("0000010", "022011")
    telediagnostico.addSolicitacao(
        "20/01/2016 22:10:12",
        "1", "17",
        "justificativa", "2653982", "01154825477", "225125", "2399741",
        "20/01/2016 22:10:12", "01154825477",
        "225125", "2399741", "06417633446", "1231", "240810"
    )

    dados_serializados = Integra.serializar(telediagnostico)
    print dados_serializados

    respostas = str(integra.enviar_dados(URL + 'api/v2/telediagnosticos/?format=json', dados_serializados))
    print respostas

def atividades_teleeducacao(integra, URL):
    """ Simula o envio da produção de participações em atividades
        Nesse exemplo é enviado os dados cadastrais de cinco atividades de tele-educação e as suas respectivas participações

        :param integra: Objeto integra, responsável por serializar os dados e enviá-los
        :type integra: Integra
        :param URL: O endereço do servidor
        :type URL: str

    """
    atividade = TeleeducacaoAtividade("0000010", "022011")
    atividade.addAtividade("01", "01/01/2016 18:00:00", "20", TipoAtividade.CURSO, "C02.782.350.250.214")
    atividade.addAtividade("02", "02/01/2016 18:00:00", "30", TipoAtividade.FORUM, "B01.050.500.131.617.289.275.100")
    atividade.addAtividade("03", "03/01/2016 18:00:00", "40", TipoAtividade.REUNIAO, "C13.703.420.491")
    atividade.addAtividade("04", "04/01/2016 18:00:00", "50", TipoAtividade.WEBAULAS_PALESTRAS, "C02.081.980")
    atividade.addAtividade("05", "05/01/2016 18:00:00", "60", TipoAtividade.WEBSEMINARIOS, "C16.131.666.507.500")
    atividade.addParticipacaoAtividade("01", "01/01/2016 18:00:00", "03450628410", "515105", "2408236", "0001465562", GrauSatisfacao.INDIFERENTE)
    atividade.addParticipacaoAtividade("01", "02/01/2016 18:00:00", "27172872487", "223293", "2408236", "0000112658", GrauSatisfacao.INSATISFEITO)
    atividade.addParticipacaoAtividade("02", "03/01/2016 18:00:00", "01097944433", "322205", "2653982", None, GrauSatisfacao.MUITO_INSATISFEITO)
    atividade.addParticipacaoAtividade("02", "04/01/2016 18:00:00", "01097944433", "322205", "2653982", None, GrauSatisfacao.MUITO_SATISFEITO)
    atividade.addParticipacaoAtividade("03", "05/01/2016 18:00:00", "91405963468", "223268", "2409011", None, GrauSatisfacao.NAO_INFORMADO)
    atividade.addParticipacaoAtividade("04", "06/01/2016 18:00:00", "03375447434", "225133", "2653982", None, GrauSatisfacao.SATISFEITO)

    dados_serializados = Integra.serializar(atividade)
    print dados_serializados

    respostas = str(integra.enviar_dados(URL + 'api/v2/atividades-teleeducacao/?format=json', dados_serializados))
    print respostas

def objetos_aprendizagem(integra, URL):

    """ Simula o envio da produção de objetos de aprendizagem.
        Nesse exemplo é enviado os dados cadastrais de cinco objetos de aprendizagem com seu respectivo número de acessos

        :param integra: Objeto integra, responsável por serializar os dados e enviá-los
        :type integra: Integra
        :param URL: O endereço do servidor
        :type URL: str

    """
    objeto_aprendizagem = TeleeducacaoObjetoAprendizagem("0000010", "022011")
    objeto_aprendizagem.addObjetoAprendizagem(
        "01", "05/01/2016 18:00:00",
        True, True, True, True, True,
        TipoObjetoAprendizagem.TEXTO, "C02.782.350.250.214", None, 60
    )
    objeto_aprendizagem.addObjetoAprendizagem(
        "02", "05/01/2016 18:00:00",
        False, False, False, False, False,
        TipoObjetoAprendizagem.APLICATIVOS, "B01.050.500.131.617.289.275.100", None, 0
    )
    objeto_aprendizagem.addObjetoAprendizagem(
        "03", "05/01/2016 18:00:00",
        True, True, True, True, True,
        TipoObjetoAprendizagem.IMAGENS, "C13.703.420.491", None, 150
    )
    objeto_aprendizagem.addObjetoAprendizagem(
        "04", "05/01/2016 18:00:00",
        True, True, True, True, True,
        TipoObjetoAprendizagem.JOGOS_EDUCACIONAIS, "C02.081.980", "http://eaulas.usp.br/portal/video.action?idItem=1802", 0
    )
    objeto_aprendizagem.addObjetoAprendizagem(
        "05", "05/01/2016 18:00:00",
        False, False, False, False, True,
        TipoObjetoAprendizagem.MULTIMIDIA, "C02.782.350.250.214", "http://eaulas.usp.br/portal/video.action?idItem=1802", 0
    )

    dados_serializados = Integra.serializar(objeto_aprendizagem)
    print dados_serializados

    respostas = str(integra.enviar_dados(URL + 'api/v2/objetos-aprendizagem/?format=json', dados_serializados))
    print respostas

def cursos(integra, URL):
    """ Simula o envio dos dados cadastrais de curso.

        Nesse exemplo, é simulado o envio de um curso em quatro momentos distintos:

        #. É enviado o cadastro do curso, observa-se que nesse momento não se sabe quem são os matriiculados, formados, etc.;
        #. O curso é atualizado com as informações dos alunos matriculados;
        #. O curso é atualizado com as informações dos alunos evadidos;
        #. O curso é atualizado com as informações dos alunos reprovados;

    :param integra: Objeto integra, responsável por serializar os dados e enviá-los
    :type integra: Integra
    :param URL: O endereço do servidor
    :type URL: str
    """
    curso = TeleeducacaoCurso("0000010", "022011")
    curso.addCurso("1234", "05/01/2016 20:00:00", "20/01/2016 20:00:00", "50", "C02.782.350.250.214", "20", [], [], [], [])
    curso.addCurso("1234", "05/01/2016 20:00:00", "20/01/2016 20:00:00", "50", "C02.782.350.250.214", "20", ["65453409215", "12201854777"], ["02659287104", "06698401650"], [], [])
    curso.addCurso("1234", "05/01/2016 20:00:00", "20/01/2016 20:00:00", "50", "C02.782.350.250.214", "20", ["49206079468"], [], ["02733890921", "04596473501"], [])
    curso.addCurso("1234", "05/01/2016 20:00:00", "20/01/2016 20:00:00", "50", "C02.782.350.250.214", "20", ["49206079468"], [], [], ["80018289215", "13178547304"])

    dados_serializados = Integra.serializar(curso)
    print dados_serializados

    respostas = str(integra.enviar_dados(URL + 'api/v2/cursos-teleeducacao/?format=json', dados_serializados))
    print respostas

if __name__ == "__main__":
    main()
